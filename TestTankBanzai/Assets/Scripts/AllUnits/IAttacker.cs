﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAttacker
{
    void Attack();
    void Init(Transform gunsRespawn);
    void SwitchOff(bool isActive);
}
