﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UnitHealth 
{
    public Action<float> OnChangeHp;

    private float maxHealth;

    private float health;
    public float Health
    {
        get
        {
            if (health < 0)
            {
                return 0;
            }
            if (health > maxHealth)
                return maxHealth;
            return health;
        }
        private set { }
    }

    private float defence;

    public UnitHealth(float _maxHelth, float _defence)
    {
        maxHealth = _maxHelth;
        defence = _defence;
        health = maxHealth;
    }

    public void ApplyDamage(float dmg)
    {
        health -= dmg * (1 - defence);

        if (OnChangeHp != null && Health > 0)
        {
            OnChangeHp.Invoke(Health / maxHealth);
        }        
    }
}
