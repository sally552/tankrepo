﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new profile", menuName ="Create/New profile")]
public class UnitProfile : ScriptableObject
{
    [Header("Жизнь")]
    public float Health;

    [Space]
    [Header("Защита")]
    [Range(0, 1)]
    public float Defence;

    [Space]
    [Header("Скорость")]
    public float MoveSpeed;

    [Space]
    [Header("Скорость поворота")]
    public float RotateSpeed;
}
