﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovedUnits
{
    void GetSpeedParam(float _speed, float _rotate);
}
