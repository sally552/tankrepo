﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;


public class UnitController : MonoBehaviour
{
    [SerializeField]
    private UnitProfile profile;

    public Action OnDeath;

    private IMovedUnits unitMoveble;
    public UnitHealth unitHealth { get; protected set; }

    private void Awake()
    {
        unitMoveble = GetComponent<IMovedUnits>();
        if(unitMoveble != null)
            unitMoveble.GetSpeedParam(profile.MoveSpeed, profile.RotateSpeed);
        unitHealth = new UnitHealth(profile.Health, profile.Defence);
    }

    private void Start()
    {
        OnDeath += Death;
    }

    public  virtual void ApplyDamage(float dmg)
    {
        unitHealth.ApplyDamage(dmg);
        if (unitHealth.Health <= 0 && OnDeath != null)
        {
            OnDeath.Invoke();
        }
    }

    protected virtual void Death()
    {
        Debug.Log("танк умер, перезагрузка сцены");
        SceneManager.LoadScene(0);
    } 
}
