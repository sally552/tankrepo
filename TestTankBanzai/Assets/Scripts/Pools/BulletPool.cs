﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BulletPool 
{
    private Queue<GameObject> poolDictionary = new Queue<GameObject>();


    public void InitPool(GameObject go, float count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject obj = GameObject.Instantiate(go) as GameObject;
            obj.SetActive(false);
            poolDictionary.Enqueue(obj);
        }
    }


    public void SpawnBullet(Vector3 position, Quaternion rotation, float damage)
    {
        //здесь надо сделать расширение пула, если нет снарядов
        if (poolDictionary.Count == 0)
        {
            return;
        }

        GameObject objectSpawn = poolDictionary.Dequeue();

        objectSpawn.SetActive(true);
        objectSpawn.transform.position = position;
        objectSpawn.transform.rotation = rotation;

        IPoolObject poolObject = objectSpawn.GetComponent<IPoolObject>();
        if (poolObject != null)
        {
            poolObject.OnObjectSpawn(damage);
        }

        poolDictionary.Enqueue(objectSpawn);
    }
}

[System.Serializable]
public class Pool
{
    public GameObject prefab;
    public int size;
}

public interface IPoolObject
{
    void OnObjectSpawn(float damage);
    void SwithedActiveObject();
}
