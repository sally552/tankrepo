﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour, IPoolObject
{
    [SerializeField]
    private float speed;

    [SerializeField]
    private float timeLive = 10f;

    private float damage;

    private Transform bulletTr;

    private void Start()
    {
        bulletTr = GetComponent<Transform>();
    }

    public void OnObjectSpawn(float _damage)
    {
        damage = _damage;
        StartCoroutine(TimeLifeBullet());
    }

    public void SwithedActiveObject()
    {
        StopAllCoroutines();
        gameObject.SetActive(false);
    }

    private void Update()
    {
        bulletTr.position += bulletTr.forward * Time.deltaTime * speed;
    }


    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Enemy"))
        {
            other.GetComponent<UnitController>().ApplyDamage(damage);
            SwithedActiveObject();
        }

        if (other.gameObject.tag == "wall")
        {
            SwithedActiveObject();
        }
    }

    private IEnumerator TimeLifeBullet()
    {
        yield return new WaitForSeconds(timeLive);
        SwithedActiveObject();
    }
}
