﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttacker : MonoBehaviour
{
    private UnitController player;

    [SerializeField]
    private EnemyAttackSettings settings;

    private Transform playerTr;
    private Transform enemyTr;

    private float coolDawn;

    private void Start()
    {
        tag = "Enemy";
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<UnitController>();
        playerTr = player.GetComponent<Transform>();
        enemyTr = transform;
        coolDawn = settings.CoolDawn;
    }

    private void Update()
    {
        coolDawn += Time.deltaTime;

        if (Vector3.Distance(playerTr.position, enemyTr.position) <= settings.DistanceAttack 
            && coolDawn >= settings.CoolDawn)
        {
            player.ApplyDamage(settings.Damage);
            coolDawn = 0;
        }
    }
}
