﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy Settings", menuName ="Create/New Enemy Settings")]
public class EnemyAttackSettings : ScriptableObject
{
    public float Damage = 1;

    public float CoolDawn = 2;

    public float DistanceAttack = 2;
}
