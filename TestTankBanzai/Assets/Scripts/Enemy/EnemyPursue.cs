﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyPursue : MonoBehaviour, IMovedUnits
{
    private float moveSpeed = 5f;
    private float rotateSpeed = 120f;

    private Transform enemyTr;

    private Transform playerTr;

    private NavMeshAgent agent;

    [SerializeField]
    private float minDistance = 0.5f;

    public void GetSpeedParam(float _speed, float _rotate)
    {
        moveSpeed = _speed;
        rotateSpeed = _rotate;
    }

    private void Start()
    {
        enemyTr = GetComponent<Transform>();
        playerTr = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        agent = GetComponent<NavMeshAgent>();
        agent.speed = moveSpeed;
        agent.angularSpeed = rotateSpeed;
    }

    private void FixedUpdate()
    {
        enemyTr.LookAt(new Vector3(playerTr.position.x, enemyTr.position.y, playerTr.position.z));
        if (Vector3.Distance(playerTr.position, enemyTr.position) >= minDistance)
        {
            agent.destination = playerTr.position;
        }
    }
}
