﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    [SerializeField]
    private List<Pool> enemyesList;

    [SerializeField]
    [Header("Размер массива(сколько будет на сцене врагов)")]
    private int countEnemyes = 10;

    [SerializeField]
    [Header("территория воскрешения врагов( с пересечением территории игрока)")]
    private Transform spawnArea;
    [SerializeField]
    [Header("территория действий игрока")]
    private Transform playerArea;

    private SpawnArea spawn;

    private List<GameObject> pool = new List<GameObject>();
    private int currentEnemyesNum = -1;

    private void Awake()
    {
        spawn = new SpawnArea(spawnArea, playerArea);

        foreach (Pool p in enemyesList)
        {
            if (p.prefab.GetComponent<EnemyController>())
            {
                InitPool(p.prefab, p.size);
               
            }
        }
        for (int i = 0; i < countEnemyes; i++)
        {
            GetEnemy();
        }
    }



    public void InitPool(GameObject go, float count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject obj = Instantiate(go);
            obj.GetComponent<EnemyController>().OnDeath += () =>
            {
                GetEnemy();
            };

            obj.SetActive(false);
            pool.Add(obj);
        }
        RandomQueue();
    }

    private void GetEnemy()
    {
        if (currentEnemyesNum == pool.Count -1 )
            currentEnemyesNum = -1;
        currentEnemyesNum++;
        if (pool[currentEnemyesNum].activeSelf == false)
        {
            pool[currentEnemyesNum].SetActive(true);
            pool[currentEnemyesNum].transform.position = spawn.GetSpawnPosition();
        }
        else
        {
            GetEnemy();
        }
    }

    private void RandomQueue()
    {
        int count = pool.Count;

        while (count > 1)
        {
            int rnd = Random.Range(0, count--);
            var temp = pool[count];
            pool[count] = pool[rnd];
            pool[rnd] = temp;
        }
    }
}
