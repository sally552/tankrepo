﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : UnitController
{

    public override void ApplyDamage(float dmg)
    {
        base.ApplyDamage(dmg);
    }

    protected override void Death()
    {
        gameObject.SetActive(false);
    }
}
