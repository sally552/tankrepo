﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnArea
{
    private Transform spawnArea;
    private Transform playerArea;

    public SpawnArea(Transform _spawnAr, Transform _playerAr)
    {
        spawnArea = _spawnAr;
        playerArea = _playerAr;
    }


    public Vector3 GetSpawnPosition()
    {

        float x = Random.Range(spawnArea.position.x - spawnArea.localScale.x / 2,
                               spawnArea.position.x + spawnArea.localScale.x / 2);
        float z = Random.Range(spawnArea.position.z - spawnArea.localScale.z / 2,
                               spawnArea.position.z + spawnArea.localScale.z / 2);
        var pos = new Vector3(x, 0, z);
        if (IsPlayerSpawn(x, z))
            return pos;
        else
            return GetSpawnPosition();
    }

    private bool IsPlayerSpawn(float x, float z)
    {
        if (!(x < playerArea.position.x + playerArea.localScale.x / 2 && x > playerArea.position.x - playerArea.localScale.x / 2))
            return true;
        if (!(z < playerArea.position.z + playerArea.localScale.z / 2 && z > playerArea.position.z - playerArea.localScale.z / 2))
            return true;
        return false;
    }
}
