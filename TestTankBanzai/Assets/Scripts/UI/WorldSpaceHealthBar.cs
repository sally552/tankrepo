﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldSpaceHealthBar : MonoBehaviour
{
    [SerializeField]
    private Slider slider;
    private UnitController memberShip;
    private Transform mainCamera;
    private Transform playerTransform;

    private void Start()
    {
        memberShip = GetComponentInParent<UnitController>();
        memberShip.unitHealth.OnChangeHp += (v) =>
        {
            if (v <= 0)
            {
                return;
            }
            slider.value = v;
        };

        playerTransform = base.transform;
        mainCamera = Camera.main.transform;
    }

    private void FixedUpdate()
    {
        if (memberShip != null)
        {
            slider.transform.LookAt(mainCamera);
        }
    }
 
}
