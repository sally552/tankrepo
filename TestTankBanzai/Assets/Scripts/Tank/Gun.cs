﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New tank's gun", menuName ="Create/New Gun")]
public class Gun : ScriptableObject, IAttacker
{
    [SerializeField]
    private float damage = 10f;

    [SerializeField]
    private float coolDawn = 2f;

    public float CoolDawn { get; private set; }

    [SerializeField]
    private GameObject gunPrefab;

    private GameObject gun;

    [SerializeField]
    private Pool bulletPref;
    private BulletPool poolBullet;
     
    public void Init(Transform gunsRespawn)
    {
        gun = Instantiate(gunPrefab, gunsRespawn.position, Quaternion.identity);
        CoolDawn = coolDawn;
        gun.transform.parent = gunsRespawn;

        poolBullet = new BulletPool();
        if (bulletPref.prefab != null)
        {
            poolBullet.InitPool(bulletPref.prefab, bulletPref.size);
        }

        SwitchOff(false);
    }

    public void Attack()
    {
        if (bulletPref != null)
        { 
            poolBullet.SpawnBullet(gun.transform.position, gun.transform.rotation, damage);
        }
    }

    public void SwitchOff(bool isActive)
    {
        gun.SetActive(isActive);
    }
}
