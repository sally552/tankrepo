﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePlayer : MonoBehaviour, IMovedUnits
{
    [SerializeField]
    [Header("территория игрока(для ограничений)")]
    private Transform playerArea;

    private float moveSpeed = 5f;
    private float rotateSpeed = 20f;

    private Rigidbody playerRb;
    private Transform playerTr;

    private float forwardMove;
    private float rightMove;

    private void Awake()
    {
        tag = "Player";
    }

    private void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        playerTr = GetComponent<Transform>();
    }

    private void Update()
    {
        forwardMove = rightMove = 0;
        if (Input.GetKey(KeyCode.UpArrow))
            forwardMove = 1;
        if (Input.GetKey(KeyCode.DownArrow))
            forwardMove = -1;
        if (Input.GetKey(KeyCode.LeftArrow))
            rightMove = -1;
        if (Input.GetKey(KeyCode.RightArrow))
            rightMove = 1;

        Move(forwardMove);
        Rotates(rightMove);
    }

    private void Move(float forwadr)
    {
        Vector3 move = playerTr.forward * moveSpeed * Time.deltaTime * forwadr + playerRb.position;
        if(IsMove(move))
            playerRb.MovePosition(move);
    }

    private void Rotates(float rotate)
    {
        float angle = rotateSpeed * Time.deltaTime * rotate;
        Quaternion rotateAngle = Quaternion.Euler(0, angle, 0);

        playerRb.MoveRotation(playerRb.rotation * rotateAngle);
    }

    private bool IsMove(Vector3 newPos)
    {
        if (newPos.x > playerArea.position.x + playerArea.localScale.x / 2 ||
            newPos.x < playerArea.position.x - playerArea.localScale.x / 2)
            return false;
        if (newPos.z > playerArea.position.z + playerArea.localScale.z / 2 ||
            newPos.z < playerArea.position.z - playerArea.localScale.z / 2)
            return false;
        return true;
    }

    public void GetSpeedParam(float _speed, float _rotate)
    {
        moveSpeed = _speed;
        rotateSpeed = _rotate;
    }
}
