﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaerAttacker : MonoBehaviour
{
    [SerializeField]
    private List<Gun> guns;

    [SerializeField]
    private Transform gunSpawn;

    private Gun currentGun;

    private float coolDawn = 0;

    private int currentGunNum = 0;
    public int CurrentGunsNumber
    {
        get
        {
            if (currentGunNum >= guns.Count)
                currentGunNum = 0;
            if(currentGunNum <= -1)
                currentGunNum = guns.Count-1;
            return currentGunNum;
        }
        private set { }
    }

    private void Start()
    {
        foreach (Gun gun in guns)
        {
            gun.Init(gunSpawn);
        }
        currentGun = guns[currentGunNum];
        currentGun.SwitchOff(true);
    }

    private void Update()
    {
        coolDawn += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.X) && coolDawn >= currentGun.CoolDawn)
        {
            currentGun.Attack();
            coolDawn = 0;
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            currentGunNum--;
            ChangeGun();
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            currentGunNum++;
            ChangeGun();
        }
    }

    private void ChangeGun()
    {
        currentGun.SwitchOff(false);
        currentGun = guns[CurrentGunsNumber];
        currentGun.SwitchOff(true);
    }
}
